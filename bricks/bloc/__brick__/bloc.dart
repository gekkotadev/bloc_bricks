import "package:bloc/bloc.dart";

sealed class {{name.pascalCase()}}Event {}

class {{name.pascalCase()}}Bloc extends Bloc<{{name.pascalCase()}}Event, {{type.pascalCase()}}> {
  {{name.pascalCase()}}Bloc() : super() {
    /*
      The methods defined below are event handlers for a given specific type of
      event. Generics should be used to narrow down the type of event to handle.
     */
  }

  // Process every state change.
  @override
  void onChange(Change<{{type.pascalCase()}}> change) {
    super.onChange(change);
    // Implementation.
  }

  // Process every event.
  @override
  void onEvent(CounterEvent event) {
    super.onEvent(event);
    // Implementation.
  }

  // Process every state transition.
  @override
  void onTransition(Transition<{{name.pascalCase()}}Event, int> transition) {
    // Implementation.
    super.onTransition(transition);
  }

  // Error handler.
  @override
  void onError(Object error, StackTrace stackTrace) {
    // Implementation.
    super.onError(error, stackTrace);
  }
}
