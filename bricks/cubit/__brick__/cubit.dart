import "package:bloc/bloc.dart";

class {{name.pascalCase()}}Cubit extends Cubit<{{type.pascalCase()}}> {
  {{name.pascalCase()}}Cubit() : super();

  // Process every state change.
  @override
  void onChange(Change<{{type.pascalCase()}}> change) {
    super.onChange(change);
    // Implementation
  }

  // Error handler
  @override
  void onError(Object error, StackTrace stackTrace) {
    // Implementation
    super.onError(error, stackTrace);
  }

  /*
    The methods defined under here are used to programmatically mutate the state
    this Cubit is managing. Widgets that have subscribed to this Cubit will then
    react accordingly to the update.
   */
}
